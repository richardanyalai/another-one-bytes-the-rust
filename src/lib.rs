use wasm_bindgen::prelude::*;

#[wasm_bindgen(module = "/public/js/prnt.js")]
extern "C" {
	pub fn prnt(s: &str);
}

#[wasm_bindgen]
pub fn greet() {
	prnt("Another one\nbytes the\nRust")
}
