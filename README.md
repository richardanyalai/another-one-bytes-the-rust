# Another one bytes the rust

A simple Rust WASM project

## Requirements

A computer running GNU+Linux, `Rust`, `wasm-pack` and `npm` installed on your
computer.

## How to use

```bash
# install all of the npm dependencies
npm i

# install wasm-pack
cargo install wasm-pack

# compile the Rust code to get the binary and the bindings
npm run wasm

# serve the website
npm run host
```

## Useful resources

[Rust programming language](https://www.rust-lang.org/)  
[Rust and WebAssembly](https://rustwasm.github.io/docs/book/)  
[Compiling from Rust to WebAssembly](https://developer.mozilla.org/en-US/docs/WebAssembly/Rust_to_wasm)
